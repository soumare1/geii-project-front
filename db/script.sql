#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE user(
        user_id          Int  Auto_increment  NOT NULL ,
        user_pwd         Varchar (50) NOT NULL ,
        user_name        Varchar (50) NOT NULL ,
        user_firstname   Varchar (50) ,
        user_tel         Int ,
        user_mail        Varchar (50) NOT NULL ,
        user_address     Varchar (50) ,
        user_siret       Int ,
        created_at       Datetime NOT NULL ,
        updated_at       Datetime NOT NULL ,
        account_validity Varchar (50) NOT NULL
	,CONSTRAINT user_PK PRIMARY KEY (user_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: role
#------------------------------------------------------------

CREATE TABLE role(
        role_id    Int  Auto_increment  NOT NULL ,
        role_name  Varchar (50) NOT NULL ,
        created_at Datetime NOT NULL ,
        updated_at Datetime NOT NULL
	,CONSTRAINT role_PK PRIMARY KEY (role_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: classe
#------------------------------------------------------------

CREATE TABLE classe(
        classe_id   Int  Auto_increment  NOT NULL ,
        classe_name Varchar (50) NOT NULL ,
        created_at  Datetime NOT NULL ,
        updated_at  Datetime NOT NULL
	,CONSTRAINT classe_PK PRIMARY KEY (classe_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: offres
#------------------------------------------------------------

CREATE TABLE offres(
        offre_id   Int  Auto_increment  NOT NULL ,
        offre_name Varchar (50) NOT NULL ,
        offre_type Int NOT NULL ,
        created_at Datetime NOT NULL ,
        updated_at Datetime NOT NULL
	,CONSTRAINT offres_PK PRIMARY KEY (offre_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: matiere
#------------------------------------------------------------

CREATE TABLE matiere(
        matiere_id   Int  Auto_increment  NOT NULL ,
        matiere_name Varchar (50) NOT NULL ,
        created_at   Datetime NOT NULL ,
        updated_at   Datetime NOT NULL
	,CONSTRAINT matiere_PK PRIMARY KEY (matiere_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: support_cours
#------------------------------------------------------------

CREATE TABLE support_cours(
        sc_id         Int  Auto_increment  NOT NULL ,
        sc_nom        Varchar (50) NOT NULL ,
        sc_lien_cours Varchar (50) NOT NULL ,
        user_id       Int NOT NULL
	,CONSTRAINT support_cours_PK PRIMARY KEY (sc_id)

	,CONSTRAINT support_cours_user_FK FOREIGN KEY (user_id) REFERENCES user(user_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user_role
#------------------------------------------------------------

CREATE TABLE user_role(
        role_id    Int NOT NULL ,
        user_id    Int NOT NULL ,
        updated_at Datetime NOT NULL ,
        created_at Datetime NOT NULL
	,CONSTRAINT user_role_PK PRIMARY KEY (role_id,user_id)

	,CONSTRAINT user_role_role_FK FOREIGN KEY (role_id) REFERENCES role(role_id)
	,CONSTRAINT user_role_user0_FK FOREIGN KEY (user_id) REFERENCES user(user_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user_classe
#------------------------------------------------------------

CREATE TABLE user_classe(
        classe_id  Int NOT NULL ,
        user_id    Int NOT NULL ,
        created_at Datetime NOT NULL ,
        updated_at Datetime NOT NULL
	,CONSTRAINT user_classe_PK PRIMARY KEY (classe_id,user_id)

	,CONSTRAINT user_classe_classe_FK FOREIGN KEY (classe_id) REFERENCES classe(classe_id)
	,CONSTRAINT user_classe_user0_FK FOREIGN KEY (user_id) REFERENCES user(user_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user_offre
#------------------------------------------------------------

CREATE TABLE user_offre(
        offre_id   Int NOT NULL ,
        user_id    Int NOT NULL ,
        created_at Datetime NOT NULL ,
        updated_at Datetime NOT NULL
	,CONSTRAINT user_offre_PK PRIMARY KEY (offre_id,user_id)

	,CONSTRAINT user_offre_offres_FK FOREIGN KEY (offre_id) REFERENCES offres(offre_id)
	,CONSTRAINT user_offre_user0_FK FOREIGN KEY (user_id) REFERENCES user(user_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: classe_matiere
#------------------------------------------------------------

CREATE TABLE classe_matiere(
        matiere_id Int NOT NULL ,
        classe_id  Int NOT NULL ,
        created_at Datetime NOT NULL ,
        updated_at Datetime NOT NULL
	,CONSTRAINT classe_matiere_PK PRIMARY KEY (matiere_id,classe_id)

	,CONSTRAINT classe_matiere_matiere_FK FOREIGN KEY (matiere_id) REFERENCES matiere(matiere_id)
	,CONSTRAINT classe_matiere_classe0_FK FOREIGN KEY (classe_id) REFERENCES classe(classe_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: note
#------------------------------------------------------------

CREATE TABLE note(
        matiere_id Int NOT NULL ,
        user_id    Int NOT NULL ,
        note       Float NOT NULL ,
        created_at Datetime NOT NULL ,
        updated_at Datetime NOT NULL
	,CONSTRAINT note_PK PRIMARY KEY (matiere_id,user_id)

	,CONSTRAINT note_matiere_FK FOREIGN KEY (matiere_id) REFERENCES matiere(matiere_id)
	,CONSTRAINT note_user0_FK FOREIGN KEY (user_id) REFERENCES user(user_id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: emploi_du_temps
#------------------------------------------------------------

CREATE TABLE emploi_du_temps(
        matiere_id     Int NOT NULL ,
        classe_id      Int NOT NULL ,
        edt_jour_debut Datetime NOT NULL ,
        edt_jour_fin   Datetime NOT NULL
	,CONSTRAINT emploi_du_temps_PK PRIMARY KEY (matiere_id,classe_id)

	,CONSTRAINT emploi_du_temps_matiere_FK FOREIGN KEY (matiere_id) REFERENCES matiere(matiere_id)
	,CONSTRAINT emploi_du_temps_classe0_FK FOREIGN KEY (classe_id) REFERENCES classe(classe_id)
)ENGINE=InnoDB;

