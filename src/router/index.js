import {
    createRouter,
    createWebHistory
} from 'vue-router'

import accueil from '../views/pagesPricipale/IndexPage.vue'
import Connexion from '../views/pagesPricipale/authPage/ConnexionPage.vue'
import Inscription from '../views/pagesPricipale/authPage/InscriptionPage.vue'
import Notes from '../views/pagesEtudiant/Notes.vue'
import Entreprise from '../views/pagesEntreprise/pagePrincipaleEntreprise.vue'
import Etudiant from '../views/pagesEtudiant/pagePrincipaleEtudiant'
import FormulaireEntreprise from "@/views/pagesEntreprise/FormulaireEntreprise";
import Edt from "@/views/pagesEtudiant/EmploiDuTemps.vue";
import ProjetsTuteures from "@/views/pagesEtudiant/ProjetsTuteures";
import OffreAlternance from "@/views/pagesEtudiant/OffreAlternance";
import SuppCours from "@/views/pagesEtudiant/SuppCours";
import DepartementGEII from "@/views/pagesDepartementGEII/departementGEIIpagePrincipale";
import message from '../views/pagesEntreprise/message.vue'
import admin from '../views/pageAdmin/dashboard.vue'
import Enseignant from '../views/pagesEnseignant/pagePrincipaleEnseignant.vue'
import NotesEnseignant from '../views/pagesEnseignant/notesEnseignant.vue'
import EdtEnseignant from '../views/pagesEnseignant/edtEnseignant.vue'
import SuppDeCoursEnseignant from '../views/pagesEnseignant/suppDeCoursEnseignant.vue'

const routes = [{
        path: '/',
        name: 'accueil',
        component: accueil,
        meta: {
            title: "Geii | accueil"
        }
    },
    {
        path: '/accueil',
        name: 'accueil',
        component: accueil,
        meta: {
            title: "Geii | accueil"
        }
    },
    {
        path: '/connexion',
        name: 'Connexion',
        component: Connexion,
        meta: {
            title: "Geii | Connexion"
        }
    },
    {
        path: '/inscription',
        name: 'Inscription',
        component: Inscription,
        meta: {
            title: "Geii | Inscription"
        }
    },
    {
        path: '/entreprise',
        name: 'Entreprise',
        component: Entreprise,
        meta: {
            title: "Geii | Entreprise"
        }
    },
    {
        path: '/etudiant',
        name: 'Etudiant',
        component: Etudiant,
        meta: {
            title: "Geii | Etudiant"
        }
    },
    {
        path: '/notes',
        name: 'Notes',
        component: Notes,
        meta: {
            title: "Geii | Notes"
        }
    },
    {
        path: '/edt',
        name: 'Edt',
        component: Edt,
        meta: {title: "Geii | Edt"}
    },
    {
        path: '/projetsTuteures',
        name: 'ProjetsTuteures',
        component: ProjetsTuteures,
        meta: {title: "Geii | ProjetsTuteures"}
    },
    {
        path: '/offreAlternance',
        name: 'OffreAlternance',
        component: OffreAlternance,
        meta: {title: "Geii | OffreAlternance"}
    },
    {
        path: '/suppCours',
        name: 'SuppCours',
        component: SuppCours,
        meta: {title: "Geii | SuppCours"}
    },
    {
        path: '/formulaireEntreprise',
        name: 'FormulaireEntreprise',
        component: FormulaireEntreprise,
        meta: {
            title: "Geii | FormulaireEntreprise"
        }

    },
    {
        path: '/departementGEII',
        name: 'DepartementGEII',
        component: DepartementGEII,
        meta: {title: "Geii | DepartementGEII"}

    },

    {
        path: '/message',
        name: 'message',
        component: message,
        meta: {
            title: "Geii | message"
        }

    },
    {
        path: '/enseignant',
        name: 'Enseignant',
        component: Enseignant,
        meta: {title: "Geii | Enseignant"}

    },
    {
        path: '/notesEnseignant',
        name: 'NotesEnseignant',
        component: NotesEnseignant,
        meta: {title: "Geii | NotesEnseignant"}

    },
    {
        path: '/edtEnseignant',
        name: 'EdtEnseignant',
        component: EdtEnseignant,
        meta: {title: "Geii | EdtEnseignant"}

    },
    {
        path: '/suppDeCoursEnseignant',
        name: 'SuppDeCoursEnseignant',
        component: SuppDeCoursEnseignant,
        meta: {title: "Geii | SuppDeCoursEnseignant"}

    },
    {
        path: '/admin',
        name: 'admin',
        component: admin,
        meta: {
            title: "Geii | admin"
        }

    },


]
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})


export default router