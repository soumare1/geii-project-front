import http from "./http-common.js";
class RoleDataService {
  getAll() {
    return http.get("/role");
  }
  get(id) {
    return http.get(`/role/${id}`);
  }
}
export default new RoleDataService();
